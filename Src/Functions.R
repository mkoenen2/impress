ref.values<- function(df, k=k){
  set.seed(100)
  xMix<- gammamixEM(df$IgA_values, k=k)
  plot<-plot_mm(xMix, k=k)+ scale_x_continuous(name= "IgA values", n.breaks = 10) + ggtitle("Histogram") 
  
  fit <- gammamixEM(df$IgA_values, k=k) 
  fit.df<- as.data.frame(fit$gamma.pars)
  
  low.1<-qgamma(0.025, fit.df$comp.1[1], 1/fit.df$comp.1[2]) 
  high.1<-qgamma(0.975, fit.df$comp.1[1], 1/fit.df$comp.1[2]) 
  low.2<-qgamma(0.025, fit.df$comp.2[1], 1/fit.df$comp.2[2]) 
  high.2<-qgamma(0.975, fit.df$comp.2[1], 1/fit.df$comp.2[2]) 
  ref.1<- data.frame(Lower_limit =c(low.1, low.2), Upper_limit=c(high.1, high.2), n_total=nrow(df))
  
  if(k == "3") {
    low.3<-qgamma(0.025, fit.df$comp.3[1], 1/fit.df$comp.3[2]) 
    high.3<-qgamma(0.975, fit.df$comp.3[1], 1/fit.df$comp.3[2]) 
    ref.1<- data.frame(Lower_limit =c(low.1, low.2, low.3), Upper_limit=c(high.1, high.2, high.3),n_total=nrow(df))
  }
  if(k == "4") {
    low.3<-qgamma(0.025, fit.df$comp.3[1], 1/fit.df$comp.3[2]) 
    high.3<-qgamma(0.975, fit.df$comp.3[1], 1/fit.df$comp.3[2]) 
    low.4<-qgamma(0.025, fit.df$comp.4[1], 1/fit.df$comp.4[2]) 
    high.4<-qgamma(0.975, fit.df$comp.4[1], 1/fit.df$comp.4[2]) 
    ref.1<- data.frame(Lower_limit =c(low.1, low.2, low.3, low.4), Upper_limit=c(high.1, high.2, high.3, high.4),n_total=nrow(df))
  }
  if(k == "5") {
    low.3<-qgamma(0.025, fit.df$comp.3[1], 1/fit.df$comp.3[2]) 
    high.3<-qgamma(0.975, fit.df$comp.3[1], 1/fit.df$comp.3[2]) 
    low.4<-qgamma(0.025, fit.df$comp.4[1], 1/fit.df$comp.4[2]) 
    high.4<-qgamma(0.975, fit.df$comp.4[1], 1/fit.df$comp.4[2]) 
    low.5<-qgamma(0.025, fit.df$comp.5[1], 1/fit.df$comp.5[2]) 
    high.5<-qgamma(0.975, fit.df$comp.5[1], 1/fit.df$comp.5[2]) 
    ref.1<- data.frame(Lower_limit =c(low.1, low.2, low.3, low.4, low.5), Upper_limit=c(high.1, high.2, high.3, high.4, high.5),n_total=nrow(df))
  }
  
  
  set.seed(250)
  xMix<- gammamixEM(df$IgA_values, k=k)
  plot.2<-plot_mm(xMix, k=k)+ scale_x_continuous(name= "IgA values", n.breaks = 10) + ggtitle("Histogram") 
  
  fit <- gammamixEM(df$IgA_values, k=k) 
  fit.df_2<- as.data.frame(fit$gamma.pars)
  
  low.1<-qgamma(0.025, fit.df_2$comp.1[1], 1/fit.df_2$comp.1[2]) 
  high.1<-qgamma(0.975, fit.df_2$comp.1[1], 1/fit.df_2$comp.1[2]) 
  low.2<-qgamma(0.025, fit.df_2$comp.2[1], 1/fit.df_2$comp.2[2]) 
  high.2<-qgamma(0.975, fit.df_2$comp.2[1], 1/fit.df_2$comp.2[2]) 
  ref.2<- data.frame(Lower_limit =c(low.1, low.2), Upper_limit=c(high.1, high.2), n_total=nrow(df))
  
  if(k == "3") {
    low.3<-qgamma(0.025, fit.df_2$comp.3[1], 1/fit.df_2$comp.3[2]) 
    high.3<-qgamma(0.975, fit.df_2$comp.3[1], 1/fit.df_2$comp.3[2]) 
    ref.2<- data.frame(Lower_limit =c(low.1, low.2, low.3), Upper_limit=c(high.1, high.2, high.3),n_total=nrow(df))
  }
  if(k == "4") {
    low.3<-qgamma(0.025, fit.df_2$comp.3[1], 1/fit.df_2$comp.3[2]) 
    high.3<-qgamma(0.975, fit.df_2$comp.3[1], 1/fit.df_2$comp.3[2]) 
    low.4<-qgamma(0.025, fit.df_2$comp.4[1], 1/fit.df_2$comp.4[2]) 
    high.4<-qgamma(0.975, fit.df_2$comp.4[1], 1/fit.df_2$comp.4[2]) 
    ref.2<- data.frame(Lower_limit =c(low.1, low.2, low.3, low.4), Upper_limit=c(high.1, high.2, high.3, high.4),n_total=nrow(df))
  }
  if(k == "5") {
    low.3<-qgamma(0.025, fit.df_2$comp.3[1], 1/fit.df_2$comp.3[2]) 
    high.3<-qgamma(0.975, fit.df_2$comp.3[1], 1/fit.df_2$comp.3[2]) 
    low.4<-qgamma(0.025, fit.df_2$comp.4[1], 1/fit.df_2$comp.4[2]) 
    high.4<-qgamma(0.975, fit.df_2$comp.4[1], 1/fit.df_2$comp.4[2]) 
    low.5<-qgamma(0.025, fit.df_2$comp.5[1], 1/fit.df_2$comp.5[2]) 
    high.5<-qgamma(0.975, fit.df_2$comp.5[1], 1/fit.df_2$comp.5[2]) 
    ref.2<- data.frame(Lower_limit =c(low.1, low.2, low.3, low.4, low.5), Upper_limit=c(high.1, high.2, high.3, high.4, high.5),n_total=nrow(df))
  }
  
  set.seed(500)
  xMix<- gammamixEM(df$IgA_values, k=k)
  plot.3<-plot_mm(xMix, k=k)+ scale_x_continuous(name= "IgA values", n.breaks = 10) + ggtitle("Histogram") 
  
  fit <- gammamixEM(df$IgA_values, k=k) 
  fit.df_3<- as.data.frame(fit$gamma.pars)
  
  low.1<-qgamma(0.025, fit.df_3$comp.1[1], 1/fit.df_3$comp.1[2]) 
  high.1<-qgamma(0.975, fit.df_3$comp.1[1], 1/fit.df_3$comp.1[2]) 
  low.2<-qgamma(0.025, fit.df_3$comp.2[1], 1/fit.df_3$comp.2[2]) 
  high.2<-qgamma(0.975, fit.df_3$comp.2[1], 1/fit.df_3$comp.2[2]) 
  ref.3<- data.frame(Lower_limit =c(low.1, low.2), Upper_limit=c(high.1, high.2), n_total=nrow(df))
  
  if(k == "3") {
    low.3<-qgamma(0.025, fit.df_3$comp.3[1], 1/fit.df_3$comp.3[2]) 
    high.3<-qgamma(0.975, fit.df_3$comp.3[1], 1/fit.df_3$comp.3[2]) 
    ref.3<- data.frame(Lower_limit =c(low.1, low.2, low.3), Upper_limit=c(high.1, high.2, high.3),n_total=nrow(df))
  }
  if(k == "4") {
    low.3<-qgamma(0.025, fit.df_3$comp.3[1], 1/fit.df_3$comp.3[2]) 
    high.3<-qgamma(0.975, fit.df_3$comp.3[1], 1/fit.df_3$comp.3[2]) 
    low.4<-qgamma(0.025, fit.df_3$comp.4[1], 1/fit.df_3$comp.4[2]) 
    high.4<-qgamma(0.975, fit.df_3$comp.4[1], 1/fit.df_3$comp.4[2]) 
    ref.3<- data.frame(Lower_limit =c(low.1, low.2, low.3, low.4), Upper_limit=c(high.1, high.2, high.3, high.4),n_total=nrow(df))
  }
  if(k == "5") {
    low.3<-qgamma(0.025, fit.df_3$comp.3[1], 1/fit.df_3$comp.3[2]) 
    high.3<-qgamma(0.975, fit.df_3$comp.3[1], 1/fit.df_3$comp.3[2]) 
    low.4<-qgamma(0.025, fit.df_3$comp.4[1], 1/fit.df_3$comp.4[2]) 
    high.4<-qgamma(0.975, fit.df_3$comp.4[1], 1/fit.df_3$comp.4[2]) 
    low.5<-qgamma(0.025, fit.df_3$comp.5[1], 1/fit.df_3$comp.5[2]) 
    high.5<-qgamma(0.975, fit.df_3$comp.5[1], 1/fit.df_3$comp.5[2]) 
    ref.3<- data.frame(Lower_limit =c(low.1, low.2, low.3, low.4, low.5), Upper_limit=c(high.1, high.2, high.3, high.4, high.5),n_total=nrow(df))
  }
  
  
  set.seed(1000)
  xMix<- gammamixEM(df$IgA_values, k=k)
  plot.4<-plot_mm(xMix, k=k)+ scale_x_continuous(name= "IgA values", n.breaks = 10) + ggtitle("Histogram") 
  
  fit <- gammamixEM(df$IgA_values, k=k) 
  fit.df_4<- as.data.frame(fit$gamma.pars)
  
  low.1<-qgamma(0.025, fit.df_4$comp.1[1], 1/fit.df_4$comp.1[2]) 
  high.1<-qgamma(0.975, fit.df_4$comp.1[1], 1/fit.df_4$comp.1[2]) 
  low.2<-qgamma(0.025, fit.df_4$comp.2[1], 1/fit.df_4$comp.2[2]) 
  high.2<-qgamma(0.975, fit.df_4$comp.2[1], 1/fit.df_4$comp.2[2]) 
  ref.4<- data.frame(Lower_limit =c(low.1, low.2), Upper_limit=c(high.1, high.2), n_total=nrow(df))
  
  if(k == "3") {
    low.3<-qgamma(0.025, fit.df_4$comp.3[1], 1/fit.df_4$comp.3[2]) 
    high.3<-qgamma(0.975, fit.df_4$comp.3[1], 1/fit.df_4$comp.3[2]) 
    ref.4<- data.frame(Lower_limit =c(low.1, low.2, low.3), Upper_limit=c(high.1, high.2, high.3),n_total=nrow(df))
  }
  if(k == "4") {
    low.3<-qgamma(0.025, fit.df_4$comp.3[1], 1/fit.df_4$comp.3[2]) 
    high.3<-qgamma(0.975, fit.df_4$comp.3[1], 1/fit.df_4$comp.3[2]) 
    low.4<-qgamma(0.025, fit.df_4$comp.4[1], 1/fit.df_4$comp.4[2]) 
    high.4<-qgamma(0.975, fit.df_4$comp.4[1], 1/fit.df_4$comp.4[2]) 
    ref.4<- data.frame(Lower_limit =c(low.1, low.2, low.3, low.4), Upper_limit=c(high.1, high.2, high.3, high.4),n_total=nrow(df))
  }
  if(k == "5") {
    low.3<-qgamma(0.025, fit.df_4$comp.3[1], 1/fit.df_4$comp.3[2]) 
    high.3<-qgamma(0.975, fit.df_4$comp.3[1], 1/fit.df_4$comp.3[2]) 
    low.4<-qgamma(0.025, fit.df_4$comp.4[1], 1/fit.df_4$comp.4[2]) 
    high.4<-qgamma(0.975, fit.df_4$comp.4[1], 1/fit.df_4$comp.4[2]) 
    low.5<-qgamma(0.025, fit.df_4$comp.5[1], 1/fit.df_4$comp.5[2]) 
    high.5<-qgamma(0.975, fit.df_4$comp.5[1], 1/fit.df_4$comp.5[2]) 
    ref.4<- data.frame(Lower_limit =c(low.1, low.2, low.3, low.4, low.5), Upper_limit=c(high.1, high.2, high.3, high.4, high.5),n_total=nrow(df))
  }
  
  return(list(gamma_1=fit.df, plot_1=plot, df_1=ref.1, gamma_2=fit.df_2, plot_2=plot.2, df_2=ref.2, gamma_3=fit.df_3, plot_3=plot.3, df_3=ref.3, gamma_4=fit.df_4, plot_4=plot.4, df_4=ref.4))
} 



ref.method<- function(df.0=df.0, df.1=df.1, df.2=df.2, df.3=df.3, df.4=df.4, df.5=df.5, df.6=df.6, 
                      df.0.ref.1, df.0.ref.2, df.0.ref.3, df.0.ref.4,
                      df.1.ref.1, df.1.ref.2, df.1.ref.3, df.1.ref.4,
                      df.2.ref.1, df.2.ref.2, df.2.ref.3, df.2.ref.4,
                      df.3.ref.1, df.3.ref.2, df.3.ref.3, df.3.ref.4,
                      df.4.ref.1, df.4.ref.2, df.4.ref.3, df.4.ref.4,
                      df.5.ref.1, df.5.ref.2, df.5.ref.3, df.5.ref.4, 
                      df.6.ref.1, df.6.ref.2, df.6.ref.3, df.6.ref.4)
  {  Age_groups<- c(0,1,2,3,4,5,6)
    Lower_limit<-c(
    mean(
      df.0$df_1$Lower_limit[df.0.ref.1], 
      df.0$df_2$Lower_limit[df.0.ref.2], 
      df.0$df_3$Lower_limit[df.0.ref.3], 
      df.0$df_4$Lower_limit[df.0.ref.4]), 
    mean(
      df.1$df_1$Lower_limit[df.1.ref.1], 
      df.1$df_2$Lower_limit[df.1.ref.2], 
      df.1$df_3$Lower_limit[df.1.ref.3], 
      df.1$df_4$Lower_limit[df.1.ref.4]),
    mean(
      df.2$df_1$Lower_limit[df.2.ref.1], 
      df.2$df_2$Lower_limit[df.2.ref.2], 
      df.2$df_3$Lower_limit[df.2.ref.3], 
      df.2$df_4$Lower_limit[df.2.ref.4]),
    mean(
      df.3$df_1$Lower_limit[df.3.ref.1], 
      df.3$df_2$Lower_limit[df.3.ref.2], 
      df.3$df_3$Lower_limit[df.3.ref.3], 
      df.3$df_4$Lower_limit[df.3.ref.4]),
    mean(
      df.4$df_1$Lower_limit[df.4.ref.1], 
      df.4$df_2$Lower_limit[df.4.ref.2], 
      df.4$df_3$Lower_limit[df.4.ref.3], 
      df.4$df_4$Lower_limit[df.4.ref.4]),
    mean(
      df.5$df_1$Lower_limit[df.5.ref.1], 
      df.5$df_2$Lower_limit[df.5.ref.2], 
      df.5$df_3$Lower_limit[df.5.ref.3], 
      df.5$df_4$Lower_limit[df.5.ref.4]),
    mean(
      df.6$df_1$Lower_limit[df.6.ref.1], 
      df.6$df_2$Lower_limit[df.6.ref.2], 
      df.6$df_3$Lower_limit[df.6.ref.3], 
      df.6$df_4$Lower_limit[df.6.ref.4]))
    
  Upper_limit<-c(
    mean(
      df.0$df_1$Upper_limit[df.0.ref.1], 
      df.0$df_2$Upper_limit[df.0.ref.2], 
      df.0$df_3$Upper_limit[df.0.ref.3], 
      df.0$df_4$Upper_limit[df.0.ref.4]), 
    mean(
      df.1$df_1$Upper_limit[df.1.ref.1], 
      df.1$df_2$Upper_limit[df.1.ref.2], 
      df.1$df_3$Upper_limit[df.1.ref.3], 
      df.1$df_4$Upper_limit[df.1.ref.4]),
    mean(
      df.2$df_1$Upper_limit[df.2.ref.1], 
      df.2$df_2$Upper_limit[df.2.ref.2], 
      df.2$df_3$Upper_limit[df.2.ref.3], 
      df.2$df_4$Upper_limit[df.2.ref.4]),
    mean(
      df.3$df_1$Upper_limit[df.3.ref.1], 
      df.3$df_2$Upper_limit[df.3.ref.2], 
      df.3$df_3$Upper_limit[df.3.ref.3], 
      df.3$df_4$Upper_limit[df.3.ref.4]),
    mean(
      df.4$df_1$Upper_limit[df.4.ref.1], 
      df.4$df_2$Upper_limit[df.4.ref.2], 
      df.4$df_3$Upper_limit[df.4.ref.3], 
      df.4$df_4$Upper_limit[df.4.ref.4]),
    mean(
      df.5$df_1$Upper_limit[df.5.ref.1], 
      df.5$df_2$Upper_limit[df.5.ref.2], 
      df.5$df_3$Upper_limit[df.5.ref.3], 
      df.5$df_4$Upper_limit[df.5.ref.4]),
    mean(
      df.6$df_1$Upper_limit[df.6.ref.1], 
      df.6$df_2$Upper_limit[df.6.ref.2], 
      df.6$df_3$Upper_limit[df.6.ref.3], 
      df.6$df_4$Upper_limit[df.6.ref.4]))
  
ref.Siemens.limits<-data.frame(Age_groups, Lower_limit, Upper_limit)
return(ref.Siemens.limits)
}

alfa.beta<- function(df.0=df.0, df.1=df.1, df.2=df.2, df.3=df.3, df.4=df.4, df.5=df.5, df.6=df.6, 
                      df.0.ref.1=df.0.ref.1, df.0.ref.2=df.0.ref.2, df.0.ref.3=df.0.ref.3, df.0.ref.4=df.0.ref.4,
                      df.1.ref.1=df.1.ref.1, df.1.ref.2=df.1.ref.2, df.1.ref.3=df.1.ref.3, df.1.ref.4=df.1.ref.4,
                      df.2.ref.1=df.2.ref.1, df.2.ref.2=df.2.ref.2, df.2.ref.3=df.2.ref.3, df.2.ref.4=df.2.ref.4,
                      df.3.ref.1=df.3.ref.1, df.3.ref.2=df.3.ref.2, df.3.ref.3=df.3.ref.3, df.3.ref.4=df.3.ref.4,
                      df.4.ref.1=df.4.ref.1, df.4.ref.2=df.4.ref.2, df.4.ref.3=df.4.ref.3, df.4.ref.4=df.4.ref.4,
                      df.5.ref.1=df.5.ref.1, df.5.ref.2=df.5.ref.2, df.5.ref.3=df.5.ref.3, df.5.ref.4=df.5.ref.4,
                      df.6.ref.1=df.6.ref.1, df.6.ref.2=df.6.ref.2, df.6.ref.3=df.6.ref.3, df.6.ref.4=df.6.ref.4)
{  Age_groups<- c(0,1,2,3,4,5,6)
Alfa<-c(
  mean(
    df.0$gamma_1[[df.0.ref.1]][1], 
    df.0$gamma_2[[df.0.ref.2]][1], 
    df.0$gamma_3[[df.0.ref.3]][1], 
    df.0$gamma_4[[df.0.ref.4]][1]), 
  mean(
    df.1$gamma_1[[df.1.ref.1]][1], 
    df.1$gamma_2[[df.1.ref.2]][1], 
    df.1$gamma_3[[df.1.ref.3]][1], 
    df.1$gamma_4[[df.1.ref.4]][1]),
  mean(
    df.2$gamma_1[[df.2.ref.1]][1], 
    df.2$gamma_2[[df.2.ref.2]][1], 
    df.2$gamma_3[[df.2.ref.3]][1], 
    df.2$gamma_4[[df.2.ref.4]][1]),
  mean(
    df.3$gamma_1[[df.3.ref.1]][1], 
    df.3$gamma_2[[df.3.ref.2]][1], 
    df.3$gamma_3[[df.3.ref.3]][1], 
    df.3$gamma_4[[df.3.ref.4]][1]),
  mean(
    df.4$gamma_1[[df.4.ref.1]][1], 
    df.4$gamma_2[[df.4.ref.2]][1], 
    df.4$gamma_3[[df.4.ref.3]][1], 
    df.4$gamma_4[[df.4.ref.4]][1]),
  mean(
    df.5$gamma_1[[df.5.ref.1]][1], 
    df.5$gamma_2[[df.5.ref.2]][1], 
    df.5$gamma_3[[df.5.ref.3]][1], 
    df.5$gamma_4[[df.5.ref.4]][1]),
  mean(
    df.6$gamma_1[[df.6.ref.1]][1], 
    df.6$gamma_2[[df.6.ref.2]][1], 
    df.6$gamma_3[[df.6.ref.3]][1], 
    df.6$gamma_4[[df.6.ref.4]][1]))

Beta<-c(
  mean(
    df.0$gamma_1[[df.0.ref.1]][2], 
    df.0$gamma_2[[df.0.ref.2]][2], 
    df.0$gamma_3[[df.0.ref.3]][2], 
    df.0$gamma_4[[df.0.ref.4]][2]), 
  mean(
    df.1$gamma_1[[df.1.ref.1]][2], 
    df.1$gamma_2[[df.1.ref.2]][2], 
    df.1$gamma_3[[df.1.ref.3]][2], 
    df.1$gamma_4[[df.1.ref.4]][2]),
  mean(
    df.2$gamma_1[[df.2.ref.1]][2], 
    df.2$gamma_2[[df.2.ref.2]][2], 
    df.2$gamma_3[[df.2.ref.3]][2], 
    df.2$gamma_4[[df.2.ref.4]][2]),
  mean(
    df.3$gamma_1[[df.3.ref.1]][2], 
    df.3$gamma_2[[df.3.ref.2]][2], 
    df.3$gamma_3[[df.3.ref.3]][2], 
    df.3$gamma_4[[df.3.ref.4]][2]),
  mean(
    df.4$gamma_1[[df.4.ref.1]][2], 
    df.4$gamma_2[[df.4.ref.2]][2], 
    df.4$gamma_3[[df.4.ref.3]][2], 
    df.4$gamma_4[[df.4.ref.4]][2]),
  mean(
    df.5$gamma_1[[df.5.ref.1]][2], 
    df.5$gamma_2[[df.5.ref.2]][2], 
    df.5$gamma_3[[df.5.ref.3]][2], 
    df.5$gamma_4[[df.5.ref.4]][2]),
  mean(
    df.6$gamma_1[[df.6.ref.1]][2], 
    df.6$gamma_2[[df.6.ref.2]][2], 
    df.6$gamma_3[[df.6.ref.3]][2], 
    df.6$gamma_4[[df.6.ref.4]][2]))

ref.Siemens.limits<-data.frame(Age_groups, Alfa, Beta)
return(ref.Siemens.limits)
}